<?php

use Phalcon\Loader;
use Phalcon\Di;
use Phalcon\Mvc\Application;


// libraries
require __DIR__ . '/vendors/composer/vendor/autoload.php';
require __DIR__ . '/functions.php';


// environment
define('__DEBUG__', true);


// dirs
define('__ROOT__', _mkdir(__DIR__));

define('__CONFIG__', _mkdir(__ROOT__ . '/config'));
define('__CONTROLLERS__', _mkdir(__ROOT__ . '/controllers'));
define('__LIBRARY__', _mkdir(__ROOT__ . '/library')); // library
define('__MODELS__', _mkdir(__ROOT__ . '/models')); // models
define('__PUBLIC__', _mkdir(__ROOT__ . '/public'));
define('__SERVICES__', _mkdir(__ROOT__ . '/services'));
define('__VENDORS__', _mkdir(__ROOT__ . '/vendors'));
define('__VIEWS__', _mkdir(__ROOT__ . '/views'));

define('__CACHE__', _mkdir(__ROOT__ . '/cache'));
define('__VOLTCACHE__', _mkdir(__CACHE__ . '/views/volt'));
define('__MODELSMETADATACACHE__', _mkdir(__CACHE__ . '/modelsMetadata'));


// autoloader
$loader = new Loader();
$loader->registerNamespaces([
  'Bank\Controllers'  => __CONTROLLERS__,
  'Bank\Models'       => __MODELS__,
  'Bank\Library'      => __LIBRARY__,
]);
$loader->register();


// dependency injector
$di = new Di();
$di->setDefault($di);
$di->setShared('di', $di);


// services
foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator(__SERVICES__)) as $finfo) {
  if ($finfo->isDir()) continue;
  if ('php' !== $finfo->getExtension()) continue;

  require_once $finfo->getPathname();
}


// application
$app = new Application($di);
$app->useImplicitView(false);

// with xdebug
// $response = $app->handle();
// $response->send();

// w\o xdebug
try {
  $response = $application->handle();
  $response->send();

} catch (\Exception $e) {
  var_dump($e);

}
