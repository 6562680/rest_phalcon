(function ($, App) {
  function Test(selector) {
    this.dom = {};
    this.buff = {};

    this.dom.start = $(selector);
    this.dom.form = this.dom.start.find('.js-form');
    this.dom.jwt = this.dom.start.find('.js-jwt');
    this.dom.method = this.dom.start.find('.js-method');
    this.dom.id = this.dom.start.find('.js-id');
    this.dom.submit = this.dom.start.find('.js-submit');
    this.dom.output = this.dom.start.find('.js-output');

    this.dom.form.on('submit', (e) => {
      e.preventDefault();
    });

    this.beforeSend = (xhr) => {
      xhr.setRequestHeader('Authorization', 'Bearer ' + this.dom.jwt.val());
    };

    this.success = (response) => {
      this.dom.output.text(JSON.stringify(response, null, 2));
    };

    this.error = (jqXHR, textStatus, e) => {
      console.error(jqXHR, textStatus, e);
    };

    this.dom.submit.on('click', (e) => {
      let action = this.dom.start.data('action');
      let method = this.dom.method.val();
      let id = this.dom.id.val();

      switch (method) {
        case 'GET':
        case 'PUT':
        case 'DELETE':
          if (!id) {
            alert('Укажите `id`, это часть урла если что');
            return false;
          }

          action = action + '/' + id;
          break;

        case 'POST':
          break;

        default:
        case 'LIST':
          method = 'GET';
          break;
      }

      this.buff.method = method;

      $.ajax({
        url: action,
        crossDomain: true,
        type: this.buff.method,
        data: this.dom.form.serialize(),
        beforeSend: this.beforeSend,
        success: this.success,
        error: this.error,
      });
    });

    return this;
  }

  App.classes = App.classes || {};
  App.classes.Test = Test;
})(jQuery, App = window.App || {});