(function ($, App) {
  function Timer (selector) {
    this.dom = {};

    this.buff = {
      timer: null,

      seconds: 0,
      seconds_left: 0,
    };

    this.resolveCallback = (path) => {
      return path.split('.').reduce(function(prev, curr) {
        return prev ? prev[curr] : null
      }, this);
    }

    this.tick = () => {
      this.buff.seconds_left--;

      if (!this.buff.seconds_left) {
        // stop timer
        clearInterval(this.timer);

        if (this.buff.callback)
          this.resolveCallback(this.buff.callback)();
      }

      this.render();
    }

    this.forceLogout = (path) => {
      window.location.href = '/logout';
    }

    this.render = () => {
      let seconds_left = this.buff.seconds_left;

      let hours = Math.floor(seconds_left / 3600);
      seconds_left -= hours * 3600;

      let minutes = Math.floor(seconds_left / 60);
      minutes = String('00' + minutes).slice(-2);
      seconds_left -= minutes * 60;

      let seconds = seconds_left;
      seconds = String('00' + seconds).slice(-2);

      let time = [];
      if (hours) time.push(hours);
      time.push(minutes);
      time.push(seconds);

      this.dom.start.text(time.join(':'));
    }

    this.init = (selector) => {
      this.dom.start = $(selector);

      this.buff.callback = this.dom.start.data('callback') || '';
      this.buff.seconds = parseInt(this.dom.start.data('time')) || 0;
      this.buff.seconds_left = this.buff.seconds;

      // first render
      this.render();

      // tick
      clearInterval(this.timer);
      this.timer = setInterval(this.tick, 1000);
    }

    $(this.init(selector));

    return this;
  }

  App.classes = App.classes || {};
  App.classes.Timer = Timer;
})(jQuery, App = window.App || {});