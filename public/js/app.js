hljs.initHighlightingOnLoad();

let App = window.App || {};

App.classes = App.classes || {};
App.objects = App.objects || {};

// init timers
$('.js-timer').each((k, v) => {
  let time = parseInt(v.dataset.time) || 0;
  if (!time)
    return;

  App.objects.Timer = App.objects.Timer || [];
  App.objects.Timer.push(new App.classes.Timer(v));
});

// init test
$('.js-test').each((k, v) => {
  App.objects.Test = App.objects.Test || [];
  App.objects.Test.push(new App.classes.Test(v));
});

// init flatpickr
$('.js-flatpickr').flatpickr({
  dateFormat: 'Y-m-d H:i:S',
  altInput: true,
  altFormat: 'Y-m-d H:i:S',
  weekNumbers: true,
  enableTime: true,
});