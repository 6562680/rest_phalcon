<?php

namespace Bank\Controllers;

use Bank\Library\Controller;

use Bank\Library\Traits\tErrorBag;

use Bank\Models\Clients;
use Bank\Models\CustomerAccounts;

class ApiController extends Controller
{
  use tErrorBag;

  protected $_status;
  protected function setStatus($status) {
    $this->_status = intval($status);
    return $this;
  }
  protected function getStatus() {
    if (null === $this->_status)
      $this->_status = 0;

    return intval($this->_status);
  }

  protected $_data = [];
  protected function setData(array $data) {
    $this->_data = $data;
    return $this;
  }
  protected function mergeData(array $data) {
    $this->_data = array_replace($this->_data, $data);
    return $this;
  }
  protected function getData() {
    return $this->_data ?: [];
  }

  // json controller
  public function beforeExecuteRoute()
  {
    $auth = $this->di->get('auth');
    $response = $this->di->get('response');

    $response->setContentType('application/json');

    $this->processAuthMessages();

    if (!$auth->isAuthorized()) {
      $this->error('You should login first');

      $this->json();
      return false;
    }
  }

  public function afterExecuteRoute()
  {
    $this->json();
  }

  protected function json()
  {
    $dispatcher = $this->di->get('dispatcher');
    $response = $this->di->get('response');

    $data = $dispatcher->getReturnedValue();
    if (1
      && !is_null($data)
      && !is_bool($data)
      && !is_array($data)
    )
      return;

    if ($this->hasErrors())
      $this->setStatus(0);

    if (is_bool($data))
      $this->setStatus($data);

    if (is_array($data))
      $this->setData($data);

    $json = [];
    $json['status'] = $this->getStatus();
    $json['messages'] = $this->getMessages($clear = true);
    $json['data'] = [];

    if ($json['status'])
      $json['data'] = $this->getData();

    $dispatcher->setReturnedValue(json_encode($json));
  }

  public function loginAction()
  {
    $auth = $this->di->get('auth');
    $request = $this->di->get('request');
    $response = $this->di->get('response');

    $login = $request->getPost('login', null, $default = '');
    $password = $request->getPost('password', null, $default = '');

    $status = $auth->authorizeByLoginPassword($login, $password);

    if (!$status) {
      $this->processAuthMessages();
      return false;

    }

    $token = $auth->getToken();
    $auth->setHeader($token);

    $this->processAuthMessages();
    return $status;
  }


  public function clientsAction()
  {
    $clients = Clients::find();

    $data = [];
    $data['clients'] = [];
    foreach ($clients as $model) {
      $data['clients'][] = $model->toArray();
    }

    $this->setData($data);

    return $this->success('OK');
  }

  public function clientsGetAction()
  {
    $dispatcher = $this->di->get('dispatcher');

    $id = $dispatcher->getParam('id') ?: null;
    if (!$id)
      return $this->error('You should pass `id` parameter correctly');

    $client = Clients::findFirst([
      'id = "' . $id . '"'
    ]);
    if (!$client)
      return $this->error('Client not found by id: ' . $id);

    $data = [];
    $data['client'] = $client->toArray();

    $this->setData($data);

    return $this->success('OK');
  }

  public function clientsPostAction()
  {
    $request = $this->di->get('request');
    $dispatcher = $this->di->get('dispatcher');

    $data = $request->getPost();

    $dt = new \DateTime('now');
    $dt->setTimeZone(new \DateTimeZone('UTC'));

    $client = new Clients();
    $client->setDateAddDt($dt);

    if (!empty($data['login'])) $client->login = $data['login'];
    if (!empty($data['fullname'])) $client->fullname = $data['fullname'];
    if (!empty($data['password'])) $client->_password = $data['password'];

    if (false === $client->save()) {
      foreach ($client->getMessages() as $message) {
        $this->error($message->getMessage());
      }

      return false;
    }

    return $this->success('Succesfully inserted');
  }

  public function clientsPutAction()
  {
    $request = $this->di->get('request');
    $dispatcher = $this->di->get('dispatcher');

    $id = $dispatcher->getParam('id') ?: null;
    if (!$id)
      return $this->error('You should pass `id` parameter correctly');

    $client = Clients::findFirst([
      'id = "' . $id . '"'
    ]);
    if (!$client)
      return $this->error('Client is not found by id: ' . $id);

    $data = $request->getPut();

    if (!empty($data['login'])) $client->login = $data['login'];
    if (!empty($data['fullname'])) $client->fullname = $data['fullname'];
    if (!empty($data['password'])) $client->_password = $data['password'];

    if (false === $client->save()) {
      foreach ($client->getMessages() as $message) {
        $this->error($message->getMessage());
      }

      return false;
    }

    return $this->success('Succesfully updated');
  }

  public function clientsDeleteAction()
  {
    $request = $this->di->get('request');
    $dispatcher = $this->di->get('dispatcher');

    $id = $dispatcher->getParam('id') ?: null;
    if (!$id)
      return $this->error('You should pass `id` parameter correctly');

    $client = Clients::findFirst([
      'id = "' . $id . '"'
    ]);
    if (!$client)
      return $this->error('Client is not found by id: ' . $id);

    $client->delete();

    return $this->success('Succesfully deleted');
  }



  public function customerAccountsAction()
  {
    $customer_accounts = CustomerAccounts::find();

    $data = [];
    $data['clients'] = [];
    foreach ($customer_accounts as $model) {
      $data['clients'][] = $model->toArray();
    }

    $this->setData($data);

    return $this->success('OK');
  }

  public function customerAccountsGetAction()
  {
    $dispatcher = $this->di->get('dispatcher');

    $id = $dispatcher->getParam('id') ?: null;
    if (!$id)
      return $this->error('You should pass `id` parameter correctly');

    $customer_account = CustomerAccounts::findFirst([
      'id = "' . $id . '"'
    ]);
    if (!$customer_account)
      return $this->error('Client not found by id: ' . $id);

    $data = [];
    $data['customer_account'] = $customer_account->toArray();

    $this->setData($data);

    return $this->success('OK');
  }

  public function customerAccountsPostAction()
  {
    $request = $this->di->get('request');
    $dispatcher = $this->di->get('dispatcher');

    $data = $request->getPost();

    $dt = new \DateTime('now');
    $dt->setTimeZone(new \DateTimeZone('UTC'));

    $customer_account = new CustomerAccounts();
    $customer_account->setDateAddDt($dt);

    if (!empty($data['client_id'])) $customer_account->client_id = $data['client_id'];
    if (!empty($data['balance'])) $customer_account->_balance = $data['balance'];
    if (!empty($data['date_valid'])) $customer_account->_date_valid = $data['date_valid'];

    if (!empty($data['code'])) $customer_account->setCodeOriginal($data['code']);

    if (false === $customer_account->save()) {
      foreach ($customer_account->getMessages() as $message) {
        $this->error($message->getMessage());
      }

      return false;
    }

    return $this->success('Succesfully inserted');
  }

  public function customerAccountsPutAction()
  {
    $request = $this->di->get('request');
    $dispatcher = $this->di->get('dispatcher');

    $id = $dispatcher->getParam('id') ?: null;
    if (!$id)
      return $this->error('You should pass `id` parameter correctly');

    $customer_account = CustomerAccounts::findFirst([
      'id = "' . $id . '"'
    ]);
    if (!$customer_account)
      return $this->error('Customer Account is not found by id: ' . $id);

    $data = $request->getPut();

    if (!empty($data['client_id'])) $customer_account->client_id = $data['client_id'];
    if (!empty($data['balance'])) $customer_account->_balance = $data['balance'];
    if (!empty($data['date_valid'])) $customer_account->_date_valid = $data['date_valid'];

    if (!empty($data['code'])) $customer_account->setCodeOriginal($data['code']);

    if (false === $customer_account->save()) {
      foreach ($customer_account->getMessages() as $message) {
        $this->error($message->getMessage());
      }

      return false;
    }

    return $this->success('Succesfully updated');
  }



  public function clientsCustomerAccountsAction()
  {
    $client_id = $this->dispatcher->getParam('client_id') ?: null;
    if (!$client_id)
      return $this->error('You should pass `client_id` parameter correctly');

    $client = Clients::findFirst([
      'id = "' . $client_id . '"'
    ]);
    if (!$client)
      return $this->error('Client not found by id: ' . $client_id);

    $data = [];
    $data['customer_accounts'] = [];
    foreach ($client->CustomerAccounts as $model) {
      $data['customer_accounts'][] = $model->toArray();
    }

    $this->setData($data);

    return $this->success('OK');
  }

  public function clientsCustomerAccountsGetAction()
  {
    $client_id = $this->dispatcher->getParam('client_id') ?: null;
    if (!$client_id)
      return $this->error('You should pass `client_id` parameter correctly');

    $client = Clients::findFirst([
      'id = "' . $client_id . '"'
    ]);
    if (!$client)
      return $this->error('Client not found by id: ' . $client_id);

    $id = $this->dispatcher->getParam('id') ?: null;
    if (!$id)
      return $this->error('You should pass `id` parameter correctly');

    $customer_account = CustomerAccounts::findFirst([
      'client_id = "' . $client_id . '"',
      'id = "' . $id . '"'
    ]);
    if (!$customer_account)
      return $this->error('Customer Account not found by id: ' . $id);

    $data = [];
    $data['customer_account'] = $customer_account->toArray();

    $this->setData($data);

    return $this->success('OK');
  }


  protected function processAuthMessages()
  {
    $auth = $this->di->get('auth');

    foreach ($auth->getMessages($clear = true, 'success') as $message) {
      $this->success($message);
    }

    foreach ($auth->getMessages($clear = true, 'error') as $message) {
      $this->error($message);
    }

    foreach ($auth->getMessages($clear = true, 'notice') as $message) {
      $this->notice($message);
    }

    foreach ($auth->getMessages($clear = true, 'warning') as $message) {
      $this->warning($message);
    }

    return true;
  }
}
