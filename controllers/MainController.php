<?php

namespace Bank\Controllers;

use Bank\Library\Controller;

use Bank\Models\CustomerAccounts;

class MainController extends Controller
{
  // event
  public function afterExecuteRoute($dispatcher)
  {
    $view = $this->di->get('view');

    $content = $dispatcher->getReturnedValue();
    if (!is_string($content))
      return;

    $layout = $view->render('controllers/main/layouts/main', [
      'header' => $this->renderHeader(),
      'content' => $content,
    ]);

    $content = $view->render('html', [
      'content' => $layout
    ]);

    $dispatcher->setReturnedValue($content);
  }

  public function indexAction()
  {
    $view = $this->di->get('view');

    return $view->render('controllers/main/index');
  }

  public function errorAction()
  {
    $view = $this->di->get('view');

    return $view->render('controllers/main/error');
  }

  public function error404Action()
  {
    $response = $this->di->get('response');
    $view = $this->di->get('view');

    $response->setStatusCode('404', 'Not found');

    return $view->render('controllers/main/error404');
  }

  public function loginAction()
  {
    $auth = $this->di->get('auth');
    $session = $this->di->get('session');
    $response = $this->di->get('response');
    $view = $this->di->get('view');

    if ($auth->isAuthorized())
      return $response->redirect([
        'for' => 'main.cabinet',
      ]);

    $login = $session->get('login', null, $remove = true);

    return $view->render('controllers/main/login', [
      'login' => $login
    ]);
  }

  public function loginPostAction()
  {
    $auth = $this->di->get('auth');
    $flashSession = $this->di->get('flashSession');
    $request = $this->di->get('request');
    $response = $this->di->get('response');
    $security = $this->di->get('security');
    $session = $this->di->get('session');

    if (!$security->checkToken()) {
      $this->flashAuthMessages();

      $flashSession->error('Unable to login, csrf token is incorrect');

      return $response->redirect([
        'for' => 'main.login',
      ]);
    }

    $login = $request->getPost('login', null, $default = '');
    $password = $request->getPost('password', null, $default = '');

    $session->set('login', $login);

    $status = $auth->authorizeByLoginPassword($login, $password);

    if (!$status) {
      $this->flashAuthMessages();

      return $response->redirect([
        'for' => 'main.login',
      ]);

    }

    $token = $auth->getToken();
    $auth->setCookie($token);
    $auth->setHeader($token);

    $this->flashAuthMessages();

    return $response->redirect([
      'for' => 'main.cabinet',
    ]);
  }

  public function logoutAction()
  {
    $auth = $this->di->get('auth');
    $cookies = $this->di->get('cookies');
    $response = $this->di->get('response');

    $auth->logout();
    $auth->removeCookie();

    $this->flashAuthMessages();

    return $response->redirect([
      'for' => 'main.index',
    ]);
  }

  public function cabinetAction()
  {
    $auth = $this->di->get('auth');
    $flashSession = $this->di->get('flashSession');
    $response = $this->di->get('response');
    $view = $this->di->get('view');

    if (!$auth->isAuthorized()) {
      $this->flashAuthMessages();

      $flashSession->error('You should login first');

      return $response->redirect([
        'for' => 'main.login'
      ]);
    }

    $client = $auth->getClient();

    $customer_accounts = [];
    foreach ($client->CustomerAccounts as $model) {
      $customer_accounts[] = $model->toArray();
    }

    return $view->render('controllers/main/cabinet', [
      'customer_accounts' => $customer_accounts,
    ]);
  }


  public function clientsTestAction()
  {
    $auth = $this->di->get('auth');
    $flashSession = $this->di->get('flashSession');
    $response = $this->di->get('response');
    $view = $this->di->get('view');

    if (!$auth->isAuthorized()) {
      $this->flashAuthMessages();

      $flashSession->error('You should login first');

      return $response->redirect([
        'for' => 'main.login'
      ]);
    }

    return $view->render('controllers/main/clients-test', [
      'token' => $auth->getToken(),
    ]);
  }


  public function customerAccountsTestAction()
  {
    $auth = $this->di->get('auth');
    $flashSession = $this->di->get('flashSession');
    $response = $this->di->get('response');
    $view = $this->di->get('view');

    if (!$auth->isAuthorized()) {
      $this->flashAuthMessages();

      $flashSession->error('You should login first');

      return $response->redirect([
        'for' => 'main.login'
      ]);
    }

    return $view->render('controllers/main/customer-accounts-test');
  }


  // header widget
  protected function renderHeader() {
    $view = $this->di->get('view');
    $router = $this->di->get('router');
    $auth = $this->di->get('auth');

    $route = $router->getMatchedRoute();
    $routeName = $route ? $router->getMatchedRoute()->getName() : 'index';

    $cols = [ 'name', 'title', 'class' ];
    $menu = [
      [ 'main.index', 'Home', null ],
      [ 'main.cabinet', 'Cabinet', null ],
      [ 'main.clientsTest', 'Test Clients', null ],
      [ 'main.customerAccountsTest', 'Test CustomerAcounts', null ],
    ];

    if (!$auth->isAuthorized())
      $menu[] = [ 'main.login', 'Login', null ];

    $menu = array_map(function ($v) use (&$cols, &$routeName) {
      $v = array_combine($cols, $v);

      if ($v['name'] === $routeName)
        $v['class'] = ' bg-primary text-white';

      return $v;
    }, $menu);

    return $view->render('widgets/header', [
      'menu' => $menu,
      'header_cabinet' => $this->renderHeaderCabinet(),
    ]);
  }

  protected function renderHeaderCabinet() {
    $auth = $this->di->get('auth');
    $view = $this->di->get('view');

    $client = null;
    $token = null;
    $lifetime = null;

    if ($auth->isAuthorized()) {
      $client = $auth->getClient();
      $token = $auth->getToken();

      $dt_now = new \DateTime('now');
      $dt_now->setTimeZone(new \DateTimeZone('UTC'));

      // clone object to save original untouched
      $dt_token = clone $token->date_add_dt;

      $lifetime = max(0, ($dt_token->getTimestamp() + $token->lifetime) - $dt_now->getTimestamp());

    }

    return $view->render('widgets/header_cabinet', [
      'client' => $client,
      'token' => $token,
      'lifetime' => $lifetime,
    ]);
  }

  protected function flashAuthMessages()
  {
    $auth = $this->di->get('auth');
    $flashSession = $this->di->get('flashSession');

    foreach ($auth->getMessages($clear = true, 'success') as $message) {
      $flashSession->success($message);
    }

    foreach ($auth->getMessages($clear = true, 'error') as $message) {
      $flashSession->error($message);
    }

    // clear notices/warnings
    $auth->clearMessages('notice');
    $auth->clearMessages('warning');

    return true;
  }
}
