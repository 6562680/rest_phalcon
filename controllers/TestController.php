<?php

namespace Bank\Controllers;

use Bank\Library\Controller;

class TestController extends Controller
{
  // cors support
  public function optionsAction()
  {
    $request = $this->di->get('request');
    $response = $this->di->get('response');

    $cors_method = $request->getHeader('Access-Control-Request-Method');

    $origin = $request->getHeader('Origin') ?: '*';
    $domain = $request->getScheme() . '://' . $request->getHttpHost();

    if (!$cors_method)
      return $response->setStatusCode(403, 'Forbidden');

    if ($origin !== $domain)
      return $response->setStatusCode(403, 'Forbidden');

    return $response->setStatusCode(200, 'OK');
  }
}
