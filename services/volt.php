<?php

use Phalcon\Mvc\View\Engine\Volt;

$di->setShared('volt', function ($view, $di) {
  $volt = new Volt($view, $di);

  $volt->setOptions([
    'compiledPath'      => __VOLTCACHE__ . DIRECTORY_SEPARATOR,
    'compiledExtension' => '.compiled',
    'compileAlways' => __DEBUG__,
  ]);

  return $volt;
});
