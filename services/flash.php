<?php

use Phalcon\Flash\Direct as FlashDirect;

$di->setShared('flash', function () {
  $flash = new FlashDirect([
    'error'   => 'alert alert-danger',
    'success' => 'alert alert-success',
    'notice'  => 'alert alert-info',
    'warning' => 'alert alert-warning',
  ]);

  return $flash;
});
