<?php

use Phalcon\Security;

$di->setShared('security', function () {
  $security = new Security();
  $security->setWorkFactor(12);

  return $security;
});
