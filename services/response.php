<?php

use Phalcon\Http\Response;

$di->setShared('response', function () {
  return new Response();
});
