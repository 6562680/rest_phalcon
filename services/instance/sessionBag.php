<?php

use Phalcon\Session\Bag;

$di->set('sessionBag', function ($key) {
  return new Bag($key);
});
