<?php

use Bank\Library\Jwt;

$di->setShared('jwt', function () {
  $jwt = new Jwt();

  // https://randomkeygen.com/
  // 504-bit WPA Key
  $jwt->setKey('`tX/gXY5Nl659^bxrxG.Nauou@dR{j$>4EBGQ>\'3D/Q0TbOP3cLqYlq?%h$,Y0;');

  return $jwt;
});
