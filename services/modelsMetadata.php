<?php

use Phalcon\Mvc\Model\MetaData\Files;

$di->setShared('modelsMetadata', function() {
  return new Files([
    'metaDataDir' => __MODELSMETADATACACHE__ . DIRECTORY_SEPARATOR,
    'lifetime' => 86400,
  ]);
});
