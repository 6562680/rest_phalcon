<?php

use Phalcon\Mvc\Router;

$di->setShared('router', function () use ($di) {
  $router = new Router(false);
  $router->removeExtraSlashes(true);

  // defaults
  $router->setDefaults([
    'namespace' => 'Bank\Controllers',
    'controller' => 'main',
    'action' => 'index',
  ]);

  // 404
  $router->notFound([
    'controller' => 'main',
    'action' => 'error404',
  ]);


  // routes
  $routes = require __CONFIG__ . '/routes.php';
  $routes = array_reverse($routes);

  $has_cors = false;
  foreach ($routes as $route) {
    list($name, $type, $path) = $route;

    $arr = explode('.', $name);
    $module = (3 === count($arr)) ? $arr[0] : null;
    $controller = ($module) ? $arr[1] : $arr[0];
    $action = ($module) ? $arr[2] : $arr[1];

    $type = explode('.', strtolower($type));

    $methods = [];

    if (in_array('*', $type)) {
      $methods = [
        'POST',
        'GET',
        'PUT',
        'PATCH',
        'HEAD',
        'DELETE',
        'OPTIONS',
        'PURGE',
        'TRACE',
        'CONNECT',
      ];

    } else {
      if (in_array('post', $type)) $methods[] = 'POST';
      if (in_array('get', $type)) $methods[] = 'GET';
      if (in_array('put', $type)) $methods[] = 'PUT';
      if (in_array('patch', $type)) $methods[] = 'PATCH';
      if (in_array('head', $type)) $methods[] = 'HEAD';
      if (in_array('delete', $type)) $methods[] = 'DELETE';
      if (in_array('options', $type)) $methods[] = 'OPTIONS';
      if (in_array('purge', $type)) $methods[] = 'PURGE';
      if (in_array('trace', $type)) $methods[] = 'TRACE';
      if (in_array('connect', $type)) $methods[] = 'CONNECT';

    }

    $is_cors = in_array('cors', $type);

    $params = [];
    if ($module) $params['module'] = $module;
    if ($controller) $params['controller'] = $controller;
    if ($action) $params['action'] = $action;

    // add route
    $route = $router
      ->add($path, $params)
      ->setName($name)
      ->via($methods)
    ;

    // add cors support
    if ($is_cors) {
      $has_cors = true;
      $route->beforeMatch(function ($uri, $route) use (&$methods) {
        $request = $this->get('request');
        $response = $this->get('response');

        $origin = $request->getHeader('Origin') ?: '*';
        $domain = $request->getScheme() . '://' . $request->getHttpHost();

        if ($origin === $domain) {
          $response
            ->setHeader("Access-Control-Allow-Origin", $origin)
            ->setHeader("Access-Control-Allow-Methods", implode(',', $methods))
            ->setHeader("Access-Control-Allow-Headers", 'Origin, X-Requested-With, Content-Range, Content-Disposition, Content-Type, Authorization, User-Agent')
            ->setHeader("Access-Control-Allow-Credentials", true)
          ;
        }

        return true;
      });

    }
  }

  // cors support
  if ($has_cors) {
    $router->addOptions('/{catch:.*}', [
      'controller' => 'test',
      'action' => 'options',
    ]);

  }

  return $router;
});
