<?php

use Phalcon\Http\Response\Cookies;

$di->setShared('cookies', function () {
  $cookies = new Cookies();
  $cookies->useEncryption(true);
  return $cookies;
});
