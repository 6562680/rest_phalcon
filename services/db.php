<?php

use Phalcon\Db\Adapter\Pdo\Mysql;

$di->setShared('db', function () {
  $config = require __CONFIG__ . '/db.php';

  return new Mysql($config);
});
