<?php

use Bank\Library\Url;

$di->setShared('url', function () use ($di) {
  $request = $di->get('request');

  $url = new Url();
  $url->setBaseUri($request->getScheme() . '://' . $request->getHttpHost() . '/');
  $url->setBasePath(__PUBLIC__ . DIRECTORY_SEPARATOR);

  return $url;
});
