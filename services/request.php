<?php

use Phalcon\Http\Request;

$di->setShared('request', function () {
  $request = new Request();
  $request->setStrictHostCheck(true);

  return $request;
});
