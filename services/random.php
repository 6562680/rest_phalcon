<?php

use Phalcon\Security\Random;

$di->setShared('random', function () {
  return new Random();
});
