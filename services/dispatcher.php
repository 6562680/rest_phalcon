<?php

use Phalcon\Mvc\Dispatcher;

$di->setShared('dispatcher', function () use ($di) {
  $dispatcher = new Dispatcher();
  $eventsManager = $di->get('eventsManager');

  // authorization by cookies/headers
  $eventsManager->attach('dispatch:beforeExecuteRoute', function () use ($di) {
    $auth = $di->get('auth');
    $auth->authorize();
  });

  $dispatcher->setEventsManager($eventsManager);

  return $dispatcher;
});
