<?php

use Bank\Library\Auth;

$di->setShared('auth', function () {
  return new Auth();
});
