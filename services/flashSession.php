<?php

use Phalcon\Flash\Session as FlashSession;

$di->setShared('flashSession', function () {
  $flashSession = new FlashSession([
    'error'   => 'alert alert-danger',
    'success' => 'alert alert-success',
    'notice'  => 'alert alert-info',
    'warning' => 'alert alert-warning',
  ]);

  return $flashSession;
});
