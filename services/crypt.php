<?php

use Phalcon\Crypt;

$di->setShared('crypt', function () {
  $crypt = new Crypt();

  // https://randomkeygen.com/
  // 504-bit WPA Key
  $key = 'H29yOe6KaQ(4:bJ"|-Q>n`U}MGx60ri<gQ@izA<A9`N=pW:J;3iOtsh8T#rc{rs';

  $crypt->setCipher('aes-256-ctr');
  $crypt->setKey($key);

  return $crypt;
});
