<?php

use Phalcon\Mvc\Model\Manager;

$di->setShared('modelsManager', function() {
  return new Manager();
});
