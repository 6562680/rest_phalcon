<?php

use Phalcon\Escaper;

$di->setShared('escaper', function () {
  return new Escaper();
});
