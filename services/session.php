<?php

use Phalcon\Session\Adapter\Files as Session;

$di->setShared('session', function () {
  $session = new Session();
  $session->start();
  return $session;
});