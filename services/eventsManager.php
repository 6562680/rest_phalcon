<?php

use Phalcon\Events\Manager as EventsManager;

$di->setShared('eventsManager', function () {
  return new EventsManager();
});
