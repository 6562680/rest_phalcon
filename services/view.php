<?php

use Phalcon\Mvc\View\Simple as View;

$di->setShared('view', function () {
  $view = new View();
  $view->setViewsDir(__VIEWS__ . DIRECTORY_SEPARATOR);

  $view->registerEngines([
    ".phtml"  => 'Phalcon\\Mvc\\View\\Engine\\Php',
    ".volt"   => 'volt',
  ]);

  return $view;
});
