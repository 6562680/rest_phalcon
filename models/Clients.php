<?php

namespace Bank\Models;

use Bank\Library\Model;

use Bank\Models\CustomerAccounts;
use Bank\Models\Tokens;

use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Uniqueness;
use Phalcon\Validation\Validator\Callback as CallbackValidator;

class Clients extends Model
{
  protected $id;
  public function getId()
  {
    return $this->id;
  }

  public $login;
  public $fullname;

  public $_password;

  protected $_date_add_dt;
  public function setDateAddDt($date_add_dt)
  {
    $this->_date_add_dt = $date_add_dt;
  }
  public function getDateAddDt()
  {
    if (null === $this->_date_add_dt) {
      if ($this->date_add) $this->_date_add_dt = \DateTime::createFromFormat('Y-m-d H:i:s', $this->date_add, new \DateTimeZone('UTC'));
    }

    return $this->_date_add_dt;
  }

  protected $password_hash;
  public function getPasswordHash()
  {
    if (null === $this->password_hash) {
      if ($this->_password) {
        $di = $this->getDi();
        $security = $di->get('security');

        $this->password_hash = $security->hash($this->_password);
      }
    }

    return $this->password_hash;
  }

  protected $date_add;
  public function getDateAdd()
  {
    if (null === $this->date_add) {
      if ($this->_date_add_dt) $this->date_add = $this->_date_add_dt->format('Y-m-d H:i:s');
    }

    return $this->date_add;
  }


  public function initialize()
  {
    $this->hasMany(
      'id',
      CustomerAccounts::class,
      'client_id',
      [ 'alias' => 'CustomerAccounts' ]
    );

    $this->hasMany(
      'id',
      Tokens::class,
      'client_id',
      [ 'alias' => 'Tokens' ]
    );
  }

  public function beforeSave()
  {
    // password_hash
    if ($this->_password) {
      $this->password_hash = null;
      $this->password_hash = $this->getPasswordHash();
    }

    // date_add
    if ($this->_date_add_dt) {
      $this->date_add = null;
      $this->date_add = $this->getDateAdd();
    }
  }


  public function validation()
  {
    $validator = new Validation();

    $validator->add('login', new PresenceOf([
      'message' => 'Field `login` is required',
    ]));
    $validator->add('login', new Uniqueness([
      'message' => 'Field `login` should be unique. Login already exists',
    ]));

    $validator->add('fullname', new PresenceOf([
      'message' => 'Field `fullname` is required',
    ]));

    $validator->add('password_hash', new CallbackValidator([
      'message' => 'Field `password` is required',
      'callback' => function ($data) {
        return $data->password_hash || $data->_password;
      },
    ]));

    $validator->add('date_add', new CallbackValidator([
      'message' => 'Field `date_add` is required',
      'callback' => function ($data) {
        return $data->date_add || $data->_date_add_dt;
      },
    ]));

    $validator->add('_date_add_dt', new CallbackValidator([
      'message' => 'Field `_date_add_dt` should be instance of DateTime',
      'callback' => function ($data) {
        if (!$data->_date_add_dt)
          return true;

        return is_a($data->_date_add_dt, \DateTime::class);
      }
    ]));

    return $this->validate($validator);
  }


  public function toArray($columns = null)
  {
    $data = [
      'id' => $this->getId(),
      'login' => $this->login,
      'fullname' => $this->fullname,
      'date_add_formatted' => _dateformat($this->date_add),
    ];

    if ($columns)
      $data = array_intersect_key($data, $columns);

    return $data;
  }
}
