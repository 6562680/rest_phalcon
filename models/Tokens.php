<?php

namespace Bank\Models;

use Bank\Library\Model;

use Bank\Model\Clients;

use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Uniqueness;
use Phalcon\Validation\Validator\Callback as CallbackValidator;

class Tokens extends Model
{
  protected $id;
  public function getId()
  {
    return $this->id;
  }

  public $client_id;
  public $ip;
  public $ip_string;
  public $user_agent;
  public $jwt;
  public $lifetime;

  protected $_date_add_dt;
  public function setDateAddDt($date_add_dt)
  {
    $this->_date_add_dt = $date_add_dt;
  }
  public function getDateAddDt()
  {
    if (null === $this->_date_add_dt) {
      if ($this->date_add) $this->_date_add_dt = \DateTime::createFromFormat('Y-m-d H:i:s', $this->date_add, new \DateTimeZone('UTC'));
    }

    return $this->_date_add_dt;
  }

  protected $date_add;
  public function getDateAdd()
  {
    if (null === $this->date_add) {
      if ($this->_date_add_dt) $this->date_add = $this->_date_add_dt->format('Y-m-d H:i:s');
    }

    return $this->date_add;
  }


  public function initialize()
  {
    $this->hasOne(
      'client_id',
      Clients::class,
      'id',
      [ 'alias' => 'Clients' ]
    );
  }

  public function beforeSave()
  {
    // date_add
    $this->date_add = $this->getDateAdd();
  }


  public function validation()
  {
    $validator = new Validation();

    $validator->add('client_id', new PresenceOf([
      'message' => 'Field `client_id` is required',
    ]));
    $validator->add('client_id', new CallbackValidator([
      'message' => 'Field `client_id` should be numeric',
      'callback' => function ($data) {
        if (!$data->client_id)
          return true;

        return is_numeric($data->client_id);
      }
    ]));

    $validator->add('ip', new PresenceOf([
      'message' => 'Field `ip` is required',
    ]));

    $validator->add('ip_string', new PresenceOf([
      'message' => 'Field `ip_string` is required',
    ]));

    $validator->add('user_agent', new PresenceOf([
      'message' => 'Field `user_agent` is required',
    ]));

    $validator->add('jwt', new PresenceOf([
      'message' => 'Field `jwt` is required',
    ]));

    $validator->add('lifetime', new PresenceOf([
      'message' => 'Field `jwt` is required',
    ]));
    $validator->add('lifetime', new CallbackValidator([
      'message' => 'Field `lifetime` should be numeric',
      'callback' => function ($data) {
        if (!$data->lifetime)
          return true;

        return is_numeric($data->lifetime);
      }
    ]));

    $validator->add('date_add', new CallbackValidator([
      'message' => 'Field `date_add` is required',
      'callback' => function ($data) {
        return $data->date_add || $data->_date_add_dt;
      },
    ]));

    $validator->add('_date_add_dt', new CallbackValidator([
      'message' => 'Field `_date_add_dt` should be instance of DateTime',
      'callback' => function ($data) {
        if (!$data->_date_add_dt)
          return true;

        return is_a($data->_date_add_dt, \DateTime::class);
      }
    ]));

    return $this->validate($validator);
  }
}
