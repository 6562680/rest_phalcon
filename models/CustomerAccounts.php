<?php

namespace Bank\Models;

use Bank\Library\Model;

use Bank\Models\Clients;

use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Uniqueness;
use Phalcon\Validation\Validator\Callback as CallbackValidator;

class CustomerAccounts extends Model
{
  protected $id;
  public function getId()
  {
    return $this->id;
  }

  public $client_id;

  protected $_code_original;
  public function setCodeOriginal($code)
  {
    $this->_code_original = $code;
  }
  public function getCodeOriginal()
  {
    if (null === $this->_code_original) {
      if ($this->code) {
        $di = $this->getDi();
        $crypt = $di->get('crypt');

        $this->_code_original = $crypt->decryptBase64($this->code);
      }
    }

    return $this->_code_original;
  }

  public $_balance;
  public $_date_valid;

  protected $_date_add_dt;
  public function setDateAddDt($date_add_dt)
  {
    $this->_date_add_dt = $date_add_dt;
  }
  public function getDateAddDt()
  {
    if (null === $this->_date_add_dt) {
      if ($this->date_add) $this->_date_add_dt = \DateTime::createFromFormat('Y-m-d H:i:s', $this->date_add, new \DateTimeZone('UTC'));
    }

    return $this->_date_add_dt;
  }


  protected $_date_valid_dt;
  public function setDateValidDt($date_valid_dt)
  {
    $this->_date_valid_dt = $date_valid_dt;
  }
  public function getDateValidDt()
  {
    if (null === $this->_date_valid_dt) {
      if ($this->_date_valid) $this->_date_valid_dt = \DateTime::createFromFormat('Y-m-d H:i:s', $this->_date_valid, new \DateTimeZone('UTC'));
      elseif ($this->date_valid) $this->_date_valid_dt = \DateTime::createFromFormat('Y-m-d H:i:s', $this->date_valid, new \DateTimeZone('UTC'));
    }

    return $this->_date_valid_dt;
  }

  protected $code;
  public function getCode()
  {
    if (null === $this->code) {
      if ($this->_code_original) {
        $di = $this->getDi();
        $crypt = $di->get('crypt');

        $this->code = $crypt->encryptBase64($this->_code_original);
      }
    }

    return $this->code;
  }

  protected $balance;
  public function getBalance()
  {
    if (null === $this->balance) {
      if ($this->_balance) {
        $this->balance = ceil($this->_balance * 100) / 100;
      }
    }

    return $this->balance;
  }

  protected $date_add;
  public function getDateAdd()
  {
    if (!$this->date_add) {
      if ($this->_date_add_dt) $this->date_add = $this->_date_add_dt->format('Y-m-d H:i:s');
    }

    return $this->date_add;
  }

  protected $date_valid;
  public function getDateValid()
  {
    if (!$this->date_valid) {
      if ($this->_date_valid_dt) $this->date_valid = $this->_date_valid_dt->format('Y-m-d H:i:s');
      elseif ($this->_date_valid) $this->date_valid = $this->_date_valid;
    }

    return $this->date_valid;
  }


  public function initialize()
  {
    $this->hasOne(
      'client_id',
      Clients::class,
      'id',
      [ 'alias' => 'Clients' ]
    );
  }

  public function beforeSave()
  {
    // code
    if ($this->_code_original) {
      $this->code = null;
      $this->code = $this->getCode();
    }

    // balance
    if ($this->_balance) {
      $this->balance = null;
      $this->balance = $this->getBalance();
    }

    // date_add
    if ($this->_date_add_dt) {
      $this->date_add = null;
      $this->date_add = $this->getDateAdd();
    }

    // date_valid
    if ($this->_date_valid || $this->_date_valid_dt) {
      $this->date_valid = null;
      $this->date_valid = $this->getDateValid();
    }
  }


  public function validation()
  {
    $validator = new Validation();

    $validator->add('client_id', new PresenceOf([
      'message' => 'Field `client_id` is required',
    ]));

    $validator->add('client_id', new CallbackValidator([
      'message' => 'Field `client_id` should be numeric',
      'callback' => function ($data) {
        if (!$data->client_id)
          return true;

        return is_numeric($data->client_id);
      },
    ]));

    $validator->add('code', new CallbackValidator([
      'message' => 'Field `code` is required',
      'callback' => function ($data) {
        return $data->code || $data->_code;
      },
    ]));
    $validator->add('code', new CallbackValidator([
      'message' => 'Field `code` should be unique for current user',
      'callback' => function ($data) {
        // get for user
        $list = CustomerAccounts::find([
          'client_id = "' . $data->client_id . '"',
        ]);

        $codes = [];
        foreach ($list as $m)
          $codes[] = $m->toArray()['code_original'];

        return !in_array($data->_code_original, $codes);
      },
    ]));

    $validator->add('balance', new CallbackValidator([
      'message' => 'Field `balance` is required',
      'callback' => function ($data) {
        return $data->balance || $data->_balance;
      },
    ]));

    $validator->add('_balance', new CallbackValidator([
      'message' => 'Field `balance` should be numeric',
      'callback' => function ($data) {
        if (!$data->_balance)
          return true;

        return is_numeric($data->_balance);
      },
    ]));

    $validator->add('date_add', new CallbackValidator([
      'message' => 'Field `date_add` is required',
      'callback' => function ($data) {
        return $data->date_add || $data->_date_add_dt;
      },
    ]));

    $validator->add('_date_add_dt', new CallbackValidator([
      'message' => 'Field `_date_add_dt` should be instance of DateTime',
      'callback' => function ($data) {
        if (!$data->_date_add_dt)
          return true;

        return is_a($data->_date_add_dt, \DateTime::class);
      },
    ]));

    $validator->add('date_valid', new PresenceOf([
      'message' => 'Field `date_valid` is required',
      'callback' => function ($data) {
        return $data->date_valid || $data->_date_valid || $data->_date_valid_dt;
      },
    ]));

    $validator->add('_date_valid', new CallbackValidator([
      'message' => 'Field `_date_valid` should be in MySQL date format',
      'callback' => function ($data) {
        if (!$data->_date_valid)
          return true;

        return !!\DateTime::createFromFormat('Y-m-d H:i:s', $data->_date_valid);
      },
    ]));

    $validator->add('_date_valid', new CallbackValidator([
      'message' => 'Field `_date_valid` should be greather than current time',
      'callback' => function ($data) {
        if (!$data->_date_valid)
          return true;

        $dt_ymdhis = \DateTime::createFromFormat('Y-m-d H:i:s', $data->_date_valid, new \DateTimeZone('UTC'));
        if (!$dt_ymdhis)
          return true;

        $dt_now = new \DateTime('now');
        $dt_now->setTimeZone(new \DateTimeZone('UTC'));

        $interval = $dt_now->diff($dt_ymdhis);

        return !$interval->invert;
      },
    ]));

    $validator->add('_date_valid_dt', new CallbackValidator([
      'message' => 'Field `_date_valid_dt` should be instance of DateTime',
      'callback' => function ($data) {
        if (!$data->_date_valid_dt)
          return true;

        return is_a($data->_date_valid_dt, \DateTime::class);
      },
    ]));

    return $this->validate($validator);
  }


  public function toArray($columns = null)
  {
    $di = $this->getDi();
    $crypt = $di->get('crypt');

    $data = [
      'id' => $this->id,
      'client_id' => $this->client_id,
      'code_original' => $this->getCodeOriginal(),
      'balance' => $this->balance,
      'date_add_formatted' => _dateformat($this->date_add),
      'date_valid_formatted' => _dateformat($this->date_valid),
    ];

    if ($columns)
      $data = array_intersect_key($data, $columns);

    return $data;
  }
}
