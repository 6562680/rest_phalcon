<div class="container">
  <h1>Customer Accounts Test</h1>

  <p>Автор сего ужаса настоятельно рекомендует использовать инструменты из магазина расширений Chrome, а не эту дичь</p>

  <div class="test js-test" data-action="/api/v1/customer_accounts">
    <form class="js-form">
      <input type="hidden" class="js-jwt" value="{{ token.jwt }}" />

      <div class="row">
        <div class="col-6">

          <div class="form-group">
            <table class="test__table table">
              <th class="test__table-th">
                Query
              </th>
              <td>
                <select class="js-method form-control rounded-0">
                  <option value="LIST">Запросить список</option>
                  <option value="GET">Одна запись</option>
                  <option value="POST">Создать запись</option>
                  <option value="PUT">Обновить запись</option>
                  <option value="DELETE">Удалить запись</option>
                </select>
              </td>
            </table>
          </div>

          <div class="form-group">
            <table class="test__table table">
              <th class="test__table-th">
                Id
              </th>
              <td>
                <input class="js-id form-control rounded-0" type="text" name="id" />
              </td>
            </table>
          </div>

          <div class="form-group">
            <table class="test__table table">
              <th class="test__table-th">
                Client Id
              </th>
              <td>
                <input class="form-control rounded-0" type="text" name="client_id" />
              </td>
            </table>
          </div>

          <div class="form-group">
            <table class="test__table table">
              <th class="test__table-th">
                Code
              </th>
              <td>
                <input class="form-control rounded-0" type="text" name="code" />
              </td>
            </table>
          </div>

          <div class="form-group">
            <table class="test__table table">
              <th class="test__table-th">
                Balance
              </th>
              <td>
                <input class="form-control rounded-0" type="text" name="balance" />
              </td>
            </table>
          </div>

          <div class="form-group">
            <table class="test__table table">
              <th class="test__table-th">
                Date valid
              </th>
              <td>
                <input class="form-control rounded-0 js-flatpickr" type="text" name="date_valid" placeholder="Click for select" />
              </td>
            </table>
          </div>

          <div class="form-group text-center">
            <button type="button" class="js-submit btn btn-primary">Отправить</button>
          </div>

        </div>
        <div class="col-6">
          <pre class="d-block"><code class="js-output json">{'hello': 'world'}</code></pre>
        </div>
      </div>
    </form>
  </div>

</div>