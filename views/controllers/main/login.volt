<div class="container">
  <div class="login w-50 mx-auto">

    <h1>Login to cabinet</h1>

    <form method="POST">
      <input type="hidden" name="{{ security.getTokenKey() }}" value="{{ security.getToken() }}" />

      <table class="login__table table table-stripped">
        <tbody>
          <tr>
            <th class="login__table-th">Login</th>
            <td class="login__table-td">
              <input type="text" name="login" class="form-control rounded-0" placeholder="login" value="{{ login }}" />
            </td>
          </tr>
          <tr>
            <th class="login__table-th">Password</th>
            <td class="login__table-td">
              <input type="password" name="password" class="form-control rounded-0" />
            </td>
          </tr>
          <tr>
            <td colspan="2" class="text-center">
              <button type="submit" class="btn btn-primary mr-1">Login</button>

              <a href="{{ url.get([
                'for': 'main.index'
              ]) }}" role="button" class="btn btn-primary">Back to index</button>

            </td>
          </tr>
        </tbody>
      </table>

    </form>

  </div>
</div>