<div class="container">
  <div class="cabinet">

    <h1>Cabinet</h1>

    <table class="cabinet__table table table-stripped table-bordered table-dark">
      <thead>
        <tr>
          <th>#</th>
          <th>Code</th>
          <th>Balance</th>
          <th>Date Added</th>
          <th>Date Valid Until</th>
        </tr>
      </thead>
      <tbody>

        {% for ca in customer_accounts %}

          <tr>
            <td>{{ ca[ 'id' ] }}</td>
            <td>{{ ca[ 'code_original' ] }}</td>
            <td>{{ ca[ 'balance' ] }}</td>
            <td>{{ ca[ 'date_add_formatted' ] }}</td>
            <td>{{ ca[ 'date_valid_formatted' ] }}</td>
          </tr>

        {% else %}

          <tr>
            <td colspan="5">No records found</td>
          </tr>

        {% endfor %}

      </tbody>
    </table>


  </div>
</div>