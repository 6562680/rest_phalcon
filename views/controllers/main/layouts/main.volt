<div class="my-3">
  {{ header }}

  <div class="container mb-2">
    {{ flashSession.output() }}
  </div>

  {{ content }}
</div>