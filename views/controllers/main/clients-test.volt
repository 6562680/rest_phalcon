<div class="container">
  <h1>Clients Test</h1>

  <p>Автор сего ужаса настоятельно рекомендует использовать инструменты из магазина расширений Chrome, а не эту дичь</p>

  <div class="test js-test" data-action="/api/v1/clients">
    <form class="js-form">
      <input type="hidden" class="js-jwt" value="{{ token.jwt }}" />

      <div class="row">
        <div class="col-6">

          <div class="form-group">
            <table class="test__table table">
              <th class="test__table-th">
                Query
              </th>
              <td>
                <select class="js-method form-control rounded-0">
                  <option value="LIST">Запросить список</option>
                  <option value="GET">Одна запись</option>
                  <option value="POST">Создать запись</option>
                  <option value="PUT">Обновить запись</option>
                  <option value="DELETE">Удалить запись</option>
                </select>
              </td>
            </table>
          </div>

          <div class="form-group">
            <table class="test__table table">
              <th class="test__table-th">
                Id
              </th>
              <td>
                <input class="js-id form-control rounded-0" type="text" name="id" />
              </td>
            </table>
          </div>

          <div class="form-group">
            <table class="test__table table">
              <th class="test__table-th">
                Login
              </th>
              <td>
                <input class="form-control rounded-0" type="text" name="login" />
              </td>
            </table>
          </div>

          <div class="form-group">
            <table class="test__table table">
              <th class="test__table-th">
                Password
              </th>
              <td>
                <input class="form-control rounded-0" type="text" name="password" />
              </td>
            </table>
          </div>

          <div class="form-group">
            <table class="test__table table">
              <th class="test__table-th">
                Fullname
              </th>
              <td>
                <input class="form-control rounded-0" type="text" name="fullname" />
              </td>
            </table>
          </div>

          <div class="form-group text-center">
            <button type="button" class="js-submit btn btn-primary">Отправить</button>
          </div>

        </div>
        <div class="col-6">
          <pre class="d-block"><code class="js-output json">{'hello': 'world'}</code></pre>
        </div>
      </div>
    </form>
  </div>

</div>