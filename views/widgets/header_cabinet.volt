{% if client %}

  <div class="d-inline-block py-2 px-3 fs-1rem">Hello, {{ client.fullname }}</div>
  <div class="d-inline-block py-2 px-3 bg-dark text-white fs-1rem">
    Session valid: <span class="js-timer" data-time="{{ lifetime }}" data-callback="forceLogout">{{ lifetime }} seconds</span>
  </div>
  <a class="d-inline-block py-2 px-3 bg-danger decoration-none text-white fs-1rem" href="{{ url.get([
    'for': 'main.logout'
  ]) }}">Logout</a>

{% endif %}