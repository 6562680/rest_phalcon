<?php

namespace Bank\Library;

use Phalcon\Mvc\Model as BaseModel;

class Model extends BaseModel
{
  protected function hasOne($fields, $referenceModel, $referencedFields, $options = null)
  {
    if (!isset($options['reusable']))
      $options['reusable'] = true;

    return $this->_modelsManager->addHasOne($this, $fields, $referenceModel, $referencedFields, $options);
  }

  protected function belongsTo($fields, $referenceModel, $referencedFields, $options = null)
  {
    if (!isset($options['reusable']))
      $options['reusable'] = true;

    return $this->_modelsManager->addBelongsTo(
      $this,
      $fields,
      $referenceModel,
      $referencedFields,
      $options
    );
  }

  protected function hasMany($fields, $referenceModel, $referencedFields, $options = null)
  {
    if (!isset($options['reusable']))
      $options['reusable'] = true;

    return $this->_modelsManager->addHasMany(
      $this,
      $fields,
      $referenceModel,
      $referencedFields,
      $options
    );
  }

  protected function hasManyToMany($fields, $intermediateModel, $intermediateFields, $intermediateReferencedFields,
    $referenceModel, $referencedFields, $options = null)
  {
    if (!isset($options['reusable']))
      $options['reusable'] = true;

    return $this->_modelsManager->addHasManyToMany(
      $this,
      $fields,
      $intermediateModel,
      $intermediateFields,
      $intermediateReferencedFields,
      $referenceModel,
      $referencedFields,
      $options
    );
  }
}
