<?php

namespace Bank\Library;

use Phalcon\Di\Injectable;

use Bank\Library\Traits\tErrorBag;

use Bank\Models\Clients;
use Bank\Models\Tokens;

Class Auth extends Injectable
{
  use tErrorBag;

  const LIFETIME = 600;

  protected $_client;
  public function getClient()
  {
    return $this->_client;
  }

  protected $_token;
  public function getToken()
  {
    return $this->_token;
  }


  public function isAuthorized()
  {
    return !!$this->_client && !!$this->_token;
  }


  public function register(string $login, string $password, string $fullname = 'User')
  {
    // find old
    $client = Clients::findFirst([
      'login = "' . $login . '"'
    ]);

    if ($client)
      return $this->error('Unable to register. Login is used already: ' . $login);

    $client = new Clients();

    $dt = new \DateTime('now');
    $dt->setTimeZone(new \DateTimeZone('UTC'));

    $client->login = $login;
    $client->password_hash = $this->security->hash($password);
    $client->fullname = $fullname;
    $client->date_add_dt = $dt;

    if (false === $client->save())
      foreach ($client->getMessages() as $message)
        $this->warning($message->getMessage());

    $this->notice('Client succesfully created');

    return true;
  }


  public function authorize()
  {
    $cookies_jwt = $this->getCookie();
    $authheader_jwt = $this->getAuthHeader();
    $header_jwt = $this->getHeader();

    if (!$cookies_jwt && !$authheader_jwt && !$header_jwt)
      return false;

    $status = false;

    if (!$status && $cookies_jwt)
      $status = $this->authorizeByJwt($cookies_jwt, $refresh_token = false);

    if (!$status && $authheader_jwt)
      $status = $this->authorizeByJwt($authheader_jwt, $refresh_token = false);

    if (!$status && $header_jwt)
      $status = $this->authorizeByJwt($header_jwt, $refresh_token = false);

    $this->clearMessages();

    return $status;
  }

  public function authorizeByLoginPassword(string $login, string $password)
  {
    $security = $this->di->get('security');

    $client = Clients::findFirst([
      'login = "' . $login . '"'
    ]);

    if (!$client) {
      // broodforce protection
      $this->security->hash(rand());

      return $this->error('Unable to authorize, incorrect login/password');
    }

    if (!$security->checkHash($password, $client->password_hash))
      return $this->error('Unable to authorize, incorrect login/password');

    $this->revokeToken($client);
    $token = $this->createToken($client);

    $this->_client = $client;
    $this->_token = $token;

    return $this->success('Succesfully logged in');
  }

  public function authorizeByJwt(string $jwt_string, bool $refresh_token = true)
  {
    $request = $this->di->get('request');
    $jwt = $this->di->get('jwt');

    $token = Tokens::findFirst([
      'jwt = "' . $jwt_string . '"',
    ]);
    if (!$token)
      return $this->error('Unable to authorize, token is not found');

    $ip = $request->getClientAddress();
    $user_agent = $request->getUserAgent();

    if ($token->ip_string !== $ip)
      return $this->error('Unable to authorize, token is incorrect for this device');

    if ($token->user_agent !== $user_agent)
      return $this->error('Unable to authorize, token is incorrect for this device');

    $client = Clients::findFirst([
      'id = "' . $token->client_id . '"'
    ]);
    if (!$client)
      return $this->error('Unable to authorize, user was removed by administrator');

    if ($this->isTokenExpired($token))
      return $this->error('Unable to authorize, token was expired');

    $jwt_client = $jwt->decode($jwt_string);
    if (!$jwt_client)
      return $this->error('Unable to authorize, incorrect jwt');

    if ($refresh_token)
      $token = $this->refreshToken($client);

    $this->_client = $client;
    $this->_token = $token;

    return $this->success('Succesfully logged in');
  }

  public function logout()
  {
    $cookies = $this->di->get('cookies');

    if ($this->_client)
      $this->revokeToken($this->_client);

    $this->_client = null;
    $this->_token = null;

    return $this->success('Succesfully logged out');
  }


  public function getCookie()
  {
    $cookies = $this->di->get('cookies');

    return $cookies->has('jwt') ? $cookies->get('jwt')->getValue() : null;
  }
  public function setCookie(Tokens $token)
  {
    $cookies = $this->di->get('cookies');

    $cookies->set('jwt', $token->jwt, time() + static::LIFETIME);

    return $this;
  }
  public function removeCookie()
  {
    $cookies = $this->di->get('cookies');

    if ($cookies->has('jwt'))
      $cookies->get('jwt')->delete();

    return $this;
  }

  public function getHeader()
  {
    $request = $this->di->get('request');

    return $request->hasHeader('X-Jwt') ? $request->getHeader('X-Jwt') : null;
  }
  public function setHeader(Tokens $token)
  {
    $response = $this->di->get('response');

    $response->setHeader('X-Jwt', $token->jwt);
  }

  public function getAuthHeader()
  {
    $request = $this->di->get('request');
    $authorization = $request->hasHeader('Authorization') ? $request->getHeader('Authorization') : null;

    $arr = explode(' ', $authorization);
    if ('Bearer' !== $arr[0])
      return null;

    if (empty($arr[1]))
      return null;

    return $arr[1];
  }


  protected function isTokenExpired(Tokens $token)
  {
    $dt_now = new \DateTime('now');
    $dt_now->setTimeZone(new \DateTimeZone('UTC'));

    // clone object to save original untouched
    $dt_token = clone $token->_date_add_dt;
    $dt_token->add(new \DateInterval('PT' . $token->lifetime . 'S'));

    $interval = $dt_now->diff($dt_token);

    return !!$interval->invert;
  }

  protected function revokeToken(Clients $client)
  {
    $request = $this->di->get('request');

    // delete tokens for this device
    $tokens = Tokens::find([
      'client_id = "' . $client->id . '"',
      'ip = "' . ip2long($request->getClientAddress()) . '"',
      'user_agent = "' . $request->getUserAgent() . '"',
    ]);

    foreach ($tokens as $token) {
      $token->delete();
    }

    return $this->notice('Tokens for this device successfully revoked');
  }

  protected function createToken(CLients $client)
  {
    $request = $this->di->get('request');
    $jwt = $this->di->get('jwt');

    // build token
    $arr = [
      'client' => $client->toArray(),
      'user_agent' => $request->getUserAgent(),
      'ip' => $request->getClientAddress(),
    ];

    // create new token
    $dt = new \DateTime('now');
    $dt->setTimeZone(new \DateTimeZone('UTC'));

    $token = new Tokens();
    $token->client_id = $client->id;
    $token->ip = ip2long($arr['ip']);
    $token->ip_string = $arr['ip'];
    $token->user_agent = $arr['user_agent'];

    // using firebase to create jwt
    $token->jwt = $jwt->encode($arr);

    $token->lifetime = static::LIFETIME;
    $token->_date_add_dt = $dt;

    if (false === $token->save())
      foreach ($token->getMessages() as $message)
        $this->warning($message->getMessage());

    $this->notice('Token succesfully created');

    return $token;
  }

  protected function refreshToken(Clients $client)
  {
    $this->revokeToken($client);
    $token = $this->createToken($client);

    $this->notice('Token succesfully refreshed');

    return $token;
  }
}
