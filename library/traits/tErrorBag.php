<?php

namespace Bank\Library\Traits;

trait tErrorBag
{
  public function message(string $type, string $message)
  {
    switch ($type):
      case 'error':
        $errors = $this->persistent->errors ?: [];
        $errors[] = $message;
        $this->persistent->errors = $errors;
        break;

      case 'success':
      case 'warning':
      case 'notice':
        break;

      default:
        $type = 'notice';
        break;
    endswitch;

    $messages = $this->persistent->messages ?: [];
    $messages[$type][] = $message;
    $this->persistent->messages = $messages;

    return true;
  }

  public function hasMessages(string $type = null)
  {
    if (is_string($type))
      return !empty($this->persistent->messages[$type]);

    return !empty($this->persistent->messages);
  }
  public function getMessages(bool $clear = false, string $type = null)
  {
    if (is_string($type)) {
      if ($this->hasMessages($type))
        $messages = $this->persistent->messages[$type];
      else
        $messages = [];

    } else {
      $messages = $this->persistent->messages ?: [];

    }

    if ($clear)
      $this->clearMessages($type);

    return $messages;
  }
  public function clearMessages(string $type = null)
  {
    if (is_string($type)) {
      if ($this->hasMessages($type)) {
        $messages = $this->persistent->messages;
        $messages[$type] = [];
        $this->persistent->messages = $messages;

      }

    } else {
      $this->persistent->messages = [];

    }

    return $this;
  }

  public function hasErrors()
  {
    return !empty($this->persistent->errors);
  }
  public function getErrors(bool $clear = false)
  {
    $errors = $this->persistent->errors ?: [];

    if ($clear)
      $this->clearErrors();

    return $errors;
  }
  public function clearErrors()
  {
    $this->persistent->errors = [];
    return $this;
  }


  public function error(string $message)
  {
    $this->message('error', $message);
    return false;
  }

  public function warning(string $message)
  {
    $this->message('warning', $message);
    return false;
  }

  public function success(string $message)
  {
    $this->message('success', $message);
    return true;
  }

  public function notice(string $message)
  {
    $this->message('notice', $message);
    return true;
  }
}
