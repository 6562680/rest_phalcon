<?php

namespace Bank\Library;

use Phalcon\Mvc\Url as BaseUrl;

class Url extends BaseUrl
{
  public function getStatic($uri = null)
  {
    $path = $this->path($uri);
    if (!is_file($path))
      throw new Exception('File not found: ' . $uri);

    return parent::getStatic($uri) . '?v=' . filemtime($path);
  }

  public function path($path = null)
  {
    return realpath(parent::path($path));
  }
}
