<?php

namespace Bank\Library;

use Firebase\JWT\JWT as FirebaseJwt;

use Phalcon\Di\Injectable;

class Jwt extends Injectable
{
  protected $_key;
  public function setKey(string $key)
  {
    $this->_key = $key;
    return $this;
  }
  public function getKey()
  {
    if (null === $this->_key)
      throw new Exception('You should call `setKey` method before');

    return $this->_key;
  }

  public function encode(array $data)
  {
    return FirebaseJwt::encode($data, $this->getKey());
  }

  public function decode(string $jwt)
  {
    return FirebaseJwt::decode($jwt, $this->getKey(), [ 'HS256' ]);
  }
}
