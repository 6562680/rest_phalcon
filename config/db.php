<?php

$charset = 'utf8mb4';

return [
  'host'     => 'localhost',
  'port'     => 3306,
  'dbname'   => 'bank',
  'username' => 'root',
  'password' => '',
  'persistent' => false,
  'options'  => [
    PDO::ATTR_ERRMODE             => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE  => PDO::FETCH_OBJ,
    PDO::ATTR_EMULATE_PREPARES    => false,
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . $charset,
  ],
];
