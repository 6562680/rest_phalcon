<?php

return [
  // [ 'test.registerClient', '*', '/test/register-client' ],
  // [ 'test.addCustomerAccount', '*', '/test/add-customer-account' ],

  [ 'main.index', '*', '/' ],
  [ 'main.error', '*', '/error' ],

  [ 'main.login', 'get', '/login' ],
  [ 'main.loginPost', 'post', '/login' ],

  [ 'main.logout', '*', '/logout' ],

  [ 'main.cabinet', '*', '/cabinet' ],
  [ 'main.clientsTest', '*', '/test/clients' ],
  [ 'main.customerAccountsTest', '*', '/test/customer_accounts' ],

  [ 'api.login', 'post.cors', '/api/v1/login' ],

  [ 'api.clients', 'get.cors', '/api/v1/clients' ], // list
  [ 'api.clientsPost', 'post.cors', '/api/v1/clients' ], // create
  [ 'api.clientsGet', 'get.cors', '/api/v1/clients/{id:[0-9]+}' ], // read
  [ 'api.clientsPut', 'put.cors', '/api/v1/clients/{id:[0-9]+}' ], // update
  [ 'api.clientsDelete', 'delete.cors', '/api/v1/clients/{id:[0-9]+}' ], // delete

  [ 'api.customerAccounts', 'get.cors', '/api/v1/customer_accounts' ],
  [ 'api.customerAccountsPost', 'post.cors', '/api/v1/customer_accounts' ],
  [ 'api.customerAccountsGet', 'get.cors', '/api/v1/customer_accounts/{id:[0-9]+}' ],
  [ 'api.customerAccountsPut', 'put.cors', '/api/v1/customer_accounts/{id:[0-9]+}' ],
  [ 'api.customerAccountsDelete', 'delete.cors', '/api/v1/customer_accounts/{id:[0-9]+}' ],

  [ 'api.clientsCustomerAccounts', 'get.cors', '/api/v1/clients/{client_id:[0-9]+}/customer_accounts' ],
  [ 'api.clientsCustomerAccountsGet', 'get.cors', '/api/v1/clients/{client_id:[0-9]+}/customer_accounts/{id:[0-9]+}' ],
];
