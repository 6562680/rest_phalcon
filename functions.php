<?php

function _mkdir(string $dp, $rights = 0755, $recursive = true)
{
  if (!is_dir($dp)) mkdir($dp, $rights, $recursive);
  return realpath($dp);
}

function _dateformat($_input = null, $_type = 'west', $_method = 'date') {
  if (!class_exists('IntlDateFormatter'))
    throw new Exception('Function _dateformat() requires php_intl module');

  if ($_input instanceof DateTime)
    $obj = $_input;
  elseif (is_string($_input) && $dt = DateTime::createFromFormat('Y-m-d H:i:s', $_input))
    $obj = $dt;
  elseif (is_numeric($_input))
    $obj = new DateTime(date('Y-m-d H:i:s', intval($_input)));
  else
    $obj = new DateTime(date('Y-m-d H:i:s', time()));

  $user_locale = Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE']) ?: 'en_US';
  $user_timezone = '+0000';
  $types = [
    'user' => [$user_locale, $user_timezone],
    'west' => ['en_US', '-0500'],
    'east' => ['ru_RU', '+0300'],
  ];
  $methods = [
    'datetime' => [IntlDateFormatter::MEDIUM, IntlDateFormatter::SHORT],
    'time' => [IntlDateFormatter::NONE, IntlDateFormatter::SHORT],
    'date' => [IntlDateFormatter::MEDIUM, IntlDateFormatter::NONE],
  ];

  if (!isset($types[$_type]))
    throw _ex('The `type`(%s) is not exists in list', $_type);

  if (!isset($methods[$_method]))
    throw _ex('The `method`(%s) is not exists in list', $_method);

  $args = [
    $types[$_type][0],
    $methods[$_method][0],
    $methods[$_method][1],
    new DateTimeZone($types[$_type][1]),
    IntlDateFormatter::GREGORIAN,
  ];

  $formatter = new IntlDateFormatter(...$args);

  return $formatter->format($obj);
}
