# Application

## authorize()
Автоматический метод authorize() отрабатывает при запуске любого скрипта, чтобы работала фронтендовая часть непосредственно на фэлконе (которая использует куки, хотя можно и хедером авторизовываться тоже)

1. RequestHeader['Authorization'] = 'Bearer [token]';
2. RequestHeader['X-Jwt'] = [token]
3. $_COOKIE['jwt'] = [token]


## POST /api/v1/login
Устанавливает заголовок X-Jwt, который потом можно сохранить

- $_POST['login']
- $_POST['password']


## GET /api/v1/clients
Выдает список клиентов в системе (всех, без пагинации)


## POST /api/v1/clients
Создает клиента

- $_POST['login']
- $_POST['password']
- $_POST['fullname']


## GET /api/v1/clients/{id:[0-9]+}
Получает клиента по ID


## PUT /api/v1/clients/{id:[0-9]+}
Обновляет клиента по ID

- $_POST['login']
- $_POST['password']
- $_POST['fullname']


## DELETE /api/v1/clients/{id:[0-9]+}
Удаляет клиента по ID


## GET /api/v1/customer_accounts
Получает список счетов в системе (всех, без пагинации)


## POST /api/v1/customer_accounts
Создает счет

- $_POST['client_id']
- $_POST['code']
- $_POST['balance']
- $_POST['date_valid']


## GET /api/v1/customer_accounts/{id:[0-9]+}
Получает счет по ID


## GET /api/v1/customer_accounts/{id:[0-9]+}
Обновляет счет по ID

- $_POST['client_id']
- $_POST['code']
- $_POST['balance']
- $_POST['date_valid']



## DELETE /api/v1/customer_accounts/{id:[0-9]+}
Удаляет счет по ID


## GET /api/v1/clients/{client_id:[0-9]+}/customer_accounts
Получает список счетов пользователя в системе (все, без пагинации)


## GET /api/v1/clients/{client_id:[0-9]+}/customer_accounts/{id:[0-9]+}
Получает счет пользователя по Id