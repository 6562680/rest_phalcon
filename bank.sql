/*
 Navicat Premium Data Transfer

 Source Server         : __localhost_root
 Source Server Type    : MySQL
 Source Server Version : 50720
 Source Host           : localhost:3306
 Source Schema         : bank

 Target Server Type    : MySQL
 Target Server Version : 50720
 File Encoding         : 65001

 Date: 22/11/2018 12:27:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for clients
-- ----------------------------
DROP TABLE IF EXISTS `clients`;
CREATE TABLE `clients`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `login` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password_hash` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `fullname` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `date_add` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of clients
-- ----------------------------
INSERT INTO `clients` VALUES (1, 'admin', '$2y$12$Y2drZDdDWjZwZGo5T241benb24jBa49m4ziTSpNCsK7H5By.2fYGq', 'Admin', '2018-11-21 13:21:24');

-- ----------------------------
-- Table structure for customer_accounts
-- ----------------------------
DROP TABLE IF EXISTS `customer_accounts`;
CREATE TABLE `customer_accounts`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `code` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `balance` decimal(10, 2) UNSIGNED NULL DEFAULT NULL,
  `date_add` datetime(0) NULL DEFAULT NULL,
  `date_valid` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  CONSTRAINT `customer_accounts_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of customer_accounts
-- ----------------------------
INSERT INTO `customer_accounts` VALUES (1, 1, '+hTdCryFJEHrrHQhkK3sYDvcbHpUDz9AeCTGOQ==', 100.00, '2018-11-21 22:10:41', '2018-12-13 12:00:00');
INSERT INTO `customer_accounts` VALUES (2, 1, 'DNeZlR6TYT9sWvf7EYAvRz3N2J5wqQ24oAwYUQ==', 55.44, '2018-11-21 22:17:29', '2018-12-05 22:17:29');
INSERT INTO `customer_accounts` VALUES (3, 1, 'qTdc25ZsabsNGGKSFNicTjY10qPGk3tBs5xEpA==', 1337.00, '2018-11-21 22:17:31', '2018-12-05 22:17:31');
INSERT INTO `customer_accounts` VALUES (4, 1, 'cYiCLRB3pADfrk4MA6ZEpnpeM7Q7vLvG1ClL/A==', 99.99, '2018-11-21 22:17:32', '2018-12-05 22:17:32');

-- ----------------------------
-- Table structure for tokens
-- ----------------------------
DROP TABLE IF EXISTS `tokens`;
CREATE TABLE `tokens`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `ip` int(10) UNSIGNED NULL DEFAULT NULL,
  `ip_string` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `user_agent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `jwt` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `date_add` datetime(0) NULL DEFAULT NULL,
  `lifetime` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  CONSTRAINT `tokens_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = InnoDB AUTO_INCREMENT = 136 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tokens
-- ----------------------------
INSERT INTO `tokens` VALUES (135, 1, 2130706433, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjbGllbnQiOnsiaWQiOjEsImxvZ2luIjoiYWRtaW4iLCJmdWxsbmFtZSI6IkFkbWluIiwiZGF0ZV9hZGRfZm9ybWF0dGVkIjoiTm92IDIxLCAyMDE4In0sInVzZXJfYWdlbnQiOiJNb3ppbGxhXC81LjAgKFdpbmRvd3MgTlQgMTAuMDsgV2luNjQ7IHg2NCkgQXBwbGVXZWJLaXRcLzUzNy4zNiAoS0hUTUwsIGxpa2UgR2Vja28pIENocm9tZVwvNzAuMC4zNTM4LjExMCBTYWZhcmlcLzUzNy4zNiIsImlwIjoiMTI3LjAuMC4xIn0.7gwb0hHfTppLcaDrgBJ2VUrLBYEDcxEwNig5PX-S8Rs', '2018-11-22 09:21:10', 600);

SET FOREIGN_KEY_CHECKS = 1;
